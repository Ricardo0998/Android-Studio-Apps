package com.example.ls12docente.ejmeneabr2018_002;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class App extends AppCompatActivity implements View.OnClickListener{
    private Button btnaceptar,btncancelar,btncerrar;
    private EditText edtname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app);
        btncancelar = (Button)findViewById(R.id.btnCancelar);
        btncerrar   = (Button)findViewById(R.id.btnCerrar);
        btncerrar.setOnClickListener(this);
        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(App.this, "Boton Cancelar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Aceptacion(View view) {

        Toast.makeText(this, "Mi tostada", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnCerrar){
            Toast.makeText(this, "Boton Cerrar", Toast.LENGTH_SHORT).show();
        }
    }
}
