package com.orbita.innovacion.sva_escuela;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash extends AppCompatActivity {

    /*Declaración de variable de tiempo de espera para hilo que checará la
    * conectividad a internet*/
    private static int SPLASH_TIME_OUT = 3500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        Chequeo();
    }

    /*Método encargado de crear hilo para la correcta función para
    * realizar el chequeo de la conectividad*/
    public void Chequeo() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (new Internet().isOnline(Splash.this)) {
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Message();
                }
            }
        }, SPLASH_TIME_OUT);

    }

    /*Método encargado de desplegar un mensaje al momento de no contar con conectividad
    * a internet*/
    private void Message() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);

        builder.setTitle("Dispositivo Sin Internet");
        builder.setMessage("Conectate a una zona con acceso a internet");

        builder.setNegativeButton("Cerrar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        finish();

                    }
                });

        builder.setPositiveButton("Reintentar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Chequeo();

                    }
                });

        builder.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
