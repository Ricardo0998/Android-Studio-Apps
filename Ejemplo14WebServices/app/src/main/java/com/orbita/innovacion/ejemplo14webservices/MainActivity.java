package com.orbita.innovacion.ejemplo14webservices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.orbita.innovacion.ejemplo14webservices.retrofit.Api;
import com.orbita.innovacion.ejemplo14webservices.retrofit.Marvel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getAll();
    }

    private void getAll(){
        //Creamos un objeto Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.URL) //URL del sitio
                .addConverterFactory(GsonConverterFactory.create()) //Libreria que comvierta de Json a Objeto
                .build(); //Ejecuta la llamada http con get y regresa los datos

        Api api = retrofit.create(Api.class);
        Call<List<Marvel>> call = api.getAll();
        call.enqueue(new Callback<List<Marvel>>() {
            @Override
            public void onResponse(Call<List<Marvel>> call, Response<List<Marvel>> response) {
                List<Marvel> datos = response.body(); //Aqui vienen los datos

                for (Marvel a: datos){
                    Log.e("Datos", a.getName());
                }
            }

            @Override
            public void onFailure(Call<List<Marvel>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
