package com.orbita.innovacion.fragmentos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.orbita.innovacion.fragmentos.Fragments.FragmentA;
import com.orbita.innovacion.fragmentos.Fragments.FragmentB;

public class MainActivity extends AppCompatActivity implements MandarDatosA, MandarDatosB{

    EditText name, pass;
    Button OK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.edtNameAct);
        pass = (EditText) findViewById(R.id.edtPassAct);
        OK = (Button) findViewById(R.id.btnOKAct);

        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarDatosA(name.getText().toString(), pass.getText().toString());
                enviarDatosB(name.getText().toString(), pass.getText().toString());
            }
        });

    }

    @Override
    public void enviarDatosA(String name, String pass) {
        FragmentB fragmentB = (FragmentB) getFragmentManager().findFragmentById(R.id.fragmentB);
        fragmentB.Recibir(name, pass);
    }

    @Override
    public void enviarDatosB(String name, String pass) {
        FragmentA fragmentA = (FragmentA) getFragmentManager().findFragmentById(R.id.fragmentA);
        fragmentA.Recibir(name, pass);
    }
}
