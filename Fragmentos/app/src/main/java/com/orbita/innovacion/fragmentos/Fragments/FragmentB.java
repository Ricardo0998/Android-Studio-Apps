package com.orbita.innovacion.fragmentos.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orbita.innovacion.fragmentos.MandarDatosB;
import com.orbita.innovacion.fragmentos.R;

public class FragmentB extends Fragment {

    EditText nam, pas;
    Button OK;
    TextView name, pass;
    MandarDatosB mandarDatosB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentb, container, false);

        name = (TextView) v.findViewById(R.id.txtNameB);
        pass = (TextView) v.findViewById(R.id.txtPassB);
        nam = (EditText) v.findViewById(R.id.edtName);
        pas = (EditText) v.findViewById(R.id.edtPass);
        OK = (Button) v.findViewById(R.id.btnOK);

        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String n = nam.getText().toString();
                String p = pas.getText().toString();

                mandarDatosB.enviarDatosB(n, p);
            }
        });

        return v;
    }

    public void Recibir(String n, String p){
        name.setText("'" + n + "'");
        pass.setText("'" + p + "'");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mandarDatosB = (MandarDatosB) activity;
    }

}
