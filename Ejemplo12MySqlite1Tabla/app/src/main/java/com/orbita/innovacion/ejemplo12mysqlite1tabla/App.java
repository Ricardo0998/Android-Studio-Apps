package com.orbita.innovacion.ejemplo12mysqlite1tabla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class App extends AppCompatActivity {

    database sqlite = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app);

        sqlite = new database(this, "", 0);
        test();

    }

    private void test(){
        Log.e(getTitle().toString(),"Insertando en la base de datos");
        sqlite.addData(new Contactos(1, "Jalil Fierro", "6141111111", false));
        sqlite.addData(new Contactos(2, "Ricardo Alderete", "614222222", false));
        sqlite.addData(new Contactos(3, "Ricardo Dos", "6143333333", false));
        sqlite.addData(new Contactos(4, "Angela Morales", "6144444444", true));

        Log.e(getTitle().toString(),"Contando los elementos en la base de datos");
        int counter = sqlite.countContactos();
        Log.e(getTitle().toString(),"Existen: " + counter + " registros en la base de datos");


    }

}
