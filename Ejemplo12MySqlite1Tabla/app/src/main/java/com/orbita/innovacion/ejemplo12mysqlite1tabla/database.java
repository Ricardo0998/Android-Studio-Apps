package com.orbita.innovacion.ejemplo12mysqlite1tabla;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Currency;

public class database extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "contactos";
    private static final String DB_TABLE = "contactos";

    // Necesito los nombres de los campos de las tablas
    //se tienen que llamar igual o  comportarseigualque os atributos del pojo

    private static final String FL_ID = "id";
    private static final String FL_NAME = "name";
    private static final String FL_PHONE = "phone";
    private static final String FL_GENRE = "gene";

    private static final String CREATE_TABLE =
                    "CREATE TABLE " + DB_TABLE + "(" + FL_ID + " INTEGER PRIMARY KEY, "
                    + FL_NAME + " TEXT, "+ FL_PHONE + "TEXT, "
                    + FL_GENRE + " INTEGER);";

    private static final String sqlGetAll = "SELECT * FROM " + DB_TABLE + ";";

    private static final String dropTable = "DROP TABLE IF EXISTS " + DB_TABLE + ";";

    public database(Context context, String name, int version) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Aqui es donde vamos a poner el create de o las tablas
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Se utiliza cuando por alguna circunstancia es necesario borrar la BD
        //para cambiarla por una version nueva
        // Siempre borramos la tabla anterios
        db.execSQL(dropTable);
        //Recreamos la tabla
        onCreate(db);
    }

    public void addData(Contactos contactos){
        //Tenemos que abrir la base de datos y decirle que vamos a escribir en ella
        //tal como si fuera un archivo
        SQLiteDatabase db = getWritableDatabase(); //Abre la base de datos creada en modo escritura
        //Necesitamos pasar los parametros para el insert, pero sqlite e android solo maneja cursores
        //por lo tanto necesitamos un objeto wrapper para almacenarlos
        ContentValues contentValues = new ContentValues();
        contentValues.put(FL_ID, contactos.getId());
        contentValues.put(FL_NAME, contactos.getNombre());
        contentValues.put(FL_PHONE, contactos.getPhone());
        //contentValues.put(FL_GENRE, (contactos.getGenero() == true)?1:0);
        //Hacemos el insert
        db.insert(DB_TABLE, null, contentValues);
        //Cerramos la base de datos
        db.close();
    }

    public void deleteData(Contactos contactos){
        //Abrimos la base de datos para escribir el ella
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DB_TABLE, FL_ID + "=?", new String[]{String.valueOf(contactos.getId())});
        db.close();
    }

    public void updateData(Contactos contactos){
        //Abrimos la base de datos para escribir el ella
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FL_NAME, contactos.getNombre());
        contentValues.put(FL_PHONE, contactos.getPhone());
        contentValues.put(FL_GENRE, (contactos.getGenero() == true)?1:0);
        db.update(DB_TABLE,contentValues, FL_ID + "=?", new String[]{String.valueOf(contactos.getId())});
        db.close();
    }

    public Contactos getContacto(int id){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_TABLE,
                new String[]{FL_ID, FL_NAME, FL_PHONE, FL_GENRE},
                FL_GENRE + "=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null);
        //Si encontro el registro el onjeto cursor es diferene de null
        Contactos contactos = null;
        if(cursor != null){
            //Nos movemos al primer registro p el unico
            cursor.moveToFirst();
            //Vaciamos el registro al objeto contacto
            contactos = new Contactos(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3) == 1);
        }
        db.close();
        return contactos;
    }

    public int countContactos(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlGetAll, null);
        int counter = cursor.getCount();
        db.close();
        return counter;
    }

    public ArrayList<Contactos> getAll(){
        //Abrir la tabla en solo lectura
        ArrayList<Contactos> datos = new ArrayList<>(); //Regresamos los datos
        SQLiteDatabase db = getReadableDatabase();
        //Pata hacer las consultas android maneja cursores
        Cursor cursor = db.rawQuery(sqlGetAll, null);
        if(cursor.moveToFirst()){
            do{
                Contactos con = new Contactos();
                con.setId(cursor.getInt(0));
                con.setNombre(cursor.getString(1));
                con.setPhone(cursor.getString(2));
                con.setGenero(cursor.getInt(3) == 1);
                datos.add(con);
            }while (cursor.moveToNext());
        }
        db.close();
        return datos;
    }

}
