package com.orbita.innovacion.proyinte;

import android.app.Dialog;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Clase encargada de mostrarse a la hora de inicir sesion con una cuenta no registrada en la BD.
 *
 * Fecha de creación: 17/02/2018.
 * Versión: 18.2.08
 * Modificaiones: Ninguna.
 */

public class NoRegistrado extends AppCompatActivity {

    private LinearLayout contacto;
    private String correo = "yagari2017@gmail.com", contrasena = "YAGARIINC2017";
    private String a = "null", b = "null";
    private TextView mensaje, address, nombre;
    private Button enviar;
    private Session session;
    private TextInputLayout Lmensaje, Laddress, Lnombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_registrado);

        contacto = (LinearLayout) findViewById(R.id.No_Registrado);
        nombre = (EditText) findViewById(R.id.edtname);
        address = (EditText) findViewById(R.id.edtcorreo);
        mensaje = (EditText) findViewById(R.id.edtmensaje);
        enviar = (Button) findViewById(R.id.btmenviar);

        a = PreferenceManager.getDefaultSharedPreferences(this).getString("Nombre","");
        b = PreferenceManager.getDefaultSharedPreferences(this).getString("Email","");
        nombre.setText(a);
        address.setText(b);

        mensaje.requestFocus();

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombre.getText().toString().equals("")){
                    Snackbar.make(contacto, "Campo de Nombre vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else if(address.getText().toString().equals("")){
                    Snackbar.make(contacto, "Campo de Correo vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else if(mensaje.getText().toString().equals("")){
                    Snackbar.make(contacto, "Campo de Mensaje vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else {
                    mandaremail();
                }
            }
        });

        enviar.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Dialog d = new Dialog(NoRegistrado.this);
                d.setContentView(R.layout.causas);
                d.show();
                return false;
            }
        });

    }

    private void mandaremail(){

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.googlemail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");

        try {
            session= Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(correo,contrasena);
                }
            });

            if(session != null){
                javax.mail.Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(correo));
                message.setSubject(a + " " + b);
                message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse("rodriguez231402@hotmail.com"));
                message.setContent(mensaje.getText().toString(), "text/html; charset=utf-8");
                Transport.send(message);

                nombre.setText(null);
                address.setText(null);
                mensaje.setText(null);
                enviar.setVisibility(View.GONE);
                Snackbar.make(contacto, "Mensaje Enviado",
                        Snackbar.LENGTH_LONG).setAction("", null).show();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
