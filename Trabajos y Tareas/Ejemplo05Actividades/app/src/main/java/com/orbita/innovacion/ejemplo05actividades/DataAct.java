package com.orbita.innovacion.ejemplo05actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DataAct extends AppCompatActivity {

    private TextView name;
    public static int OK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        name = (TextView) findViewById(R.id.txrname);

        //Traer los datos de la otra actividad
        Bundle bundle = getIntent().getExtras();
        name.setText(bundle.getString("name").toString());
    }

    private void onResponse(){
        Intent intent = new Intent();
        intent.putExtra("result", "OK");
        setResult(OK, intent);
    }

    @Override
    public void onBackPressed() {
        onResponse();
        this.finish();
        super.onBackPressed();

    }
}
