package com.orbita.innovacion.ejemplo04spinner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class App extends AppCompatActivity {

    private Spinner spn02;
    private MiSpinner miSpinner = null;
    private ArrayList<MiSpinner> list = null; //Guarda cuantas opciones voy a poner
    private ArrayAdapter<MiSpinner> adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        FillSpinner();
    }

    private void FillSpinner(){

        list = new ArrayList<>();

        list.add(new MiSpinner(0, "Selecciona..."));
        for (int i = 1;i<=5;i++){
            list.add(new MiSpinner(i,"Ejemplo " +i));
        }

        spn02 = (Spinner) findViewById(R.id.spn02);
        //adapter = new ArrayAdapter<MiSpinner>(this, android.R.layout.simple_spinner_dropdown_item, list);
        adapter = new ArrayAdapter<MiSpinner>(this, R.layout.myspinner_layout, list);

        spn02.setAdapter(adapter);

        spn02.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object papa = (MiSpinner) parent.getItemAtPosition(position);
                Integer idd = ((MiSpinner)papa).getId();
                String name = ((MiSpinner)papa).getName();
                Toast.makeText(App.this, "Name: " + name + ", Id: " + idd, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

}
